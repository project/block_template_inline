<?php

namespace Drupal\block_template_inline\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
/**
 * Edit config variable form.
 */
class ConfigTemplateEditor extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_template_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config_name = '') {

      $query = $this->getRequest()->query ;
      $config_name = $query->get('config');
      $config = \Drupal::config($config_name) ;
      $output =  $config->get('content');
      $type =  $config->get('type');
      $status_acer =  \Drupal::moduleHandler()->moduleExists('ace_editor') ;
      if($status_acer ){
          $form['content'] = [
              '#type' => 'text_format',
              '#title' => $this->t('Block Type '.$type.' Template'),
              '#format' => 'twig_code',
              '#default_value' => $output,
          ];
      }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save Template'),
        ];
        $form['actions']['cancel'] = array(
          '#type' => 'link',
          '#title' => $this->t('Back to Template list'),
          '#url' => $this->buildCancelLinkUrl(),
        );


    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $query = $this->getRequest()->query ;
      $config_name = $query->get('config');
      $values = $form_state->getValues();
      if($config_name && $values['content']){
          $content = $values['content']['value'] ;
          $this->configFactory()->getEditable($config_name)
              ->set('content',$content)
              ->save();
          $this->messenger()->addMessage($this->t('Config '.$config_name.' update  was successfully'));
      }

  }
  private function buildPreview() {

      $query = $this->getRequest()->query ;
      $config_name = $query->get('config');
      $config = \Drupal::config($config_name) ;
      $type =  $config->get('type');
      $entityQuery = \Drupal::entityQuery('block_content');
      $entityQuery->condition('type', $type);
      $entityQuery->range(0, 1);
      $blocks = $entityQuery->execute();

      $url = Url::fromRoute('block_template_inline.preview_page_controller_preview',
          ["entitype" => "block_content" , "id" => reset($blocks)]);
      return $url;
  }
  /**
   * Builds the cancel link url for the form.
   *
   * @return Url
   *   Cancel url
   */
  private function buildCancelLinkUrl() {
    $query = $this->getRequest()->query;

    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      $url = Url::fromUri('internal:/' . $options['path'], $options);
    }
    else {
      $url = Url::fromRoute('block_template_inline.manager');
    }

    return $url;
  }

}
