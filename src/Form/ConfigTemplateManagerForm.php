<?php

namespace Drupal\block_template_inline\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
/**
 * Class ContentExportSettingForm.
 */
class ConfigTemplateManagerForm extends FormBase {

//  /**
//   * {@inheritdoc}
//   */
  protected function getEditableConfigNames() {
    return [
      'block_template_inline.source',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_template_inline_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
      $query = $this->getRequest()->query;
      $config_name = "block_template_inline.settings" ;
      $config = \Drupal::config($config_name) ;
      $location =  $config->get('location');

      if($query->has('delete')){
          $delete = $query->get('delete');
              \Drupal::configFactory()->getEditable($delete)->delete();
          $this->messenger()->addMessage($this->t('Config '.$delete.' delete was successfully'));

      }
      if($query->has('deleteFile')){
          if($location){
              $delete = $query->get('deleteFile');
              $module_handler = \Drupal::service('module_handler');
              $module_path = $module_handler->getModule($location)->getPath();
              $path_module = DRUPAL_ROOT."/".$module_path."/config/install";
              $file = $path_module.'/'.$delete.'.yml' ;
              unlink($file);
              $this->messenger()->addMessage($this->t('Config '.$delete.' delete file was successfully'));
          }
          $form_state->setRedirectUrl(Url::fromRoute('block_template_inline.manager'));
      }
      if($query->has('updateFile')){
          if($location){
              $update = $query->get('updateFile');
              $module_handler = \Drupal::service('module_handler');
              $module_path = $module_handler->getModule($location)->getPath();
              $path_module = DRUPAL_ROOT."/".$module_path."/config/install";
              $config = \Drupal::config($update) ;
              $data = $config->getOriginal();
              try {
                  $output = Yaml::encode($data);
                  $file = $path_module.'/'.$update.'.yml' ;
                  file_put_contents($file, $output);
              }
              catch (InvalidDataTypeException $e) {
                  $this->messenger()->addError($this->t('Invalid data detected for @name : %error', array('@name' => $config_name, '%error' => $e->getMessage())));
                  return;
              }
              $this->messenger()->addMessage($this->t('Config '.$update.' update file was successfully'));
          }
          $form_state->setRedirectUrl(Url::fromRoute('block_template_inline.manager'));
      }

      $header = [
          'number' =>  t('ID'),
          'config_name' => t('Config name'),
          'edit' => array('data' => $this->t('Operations'))
      ];
      $output = [];
      $results = $this->configFactory()->listAll("template.");
      foreach ($results as $key => $result) {
          $operations = [] ;
          $operations['edit'] = array(
                  'title' => $this->t('Edit'),
                  'url' => Url::fromRoute('block_template_inline.editor', array('config'=>$result))
          );
          $operations['updateFile'] = array(
              'title' => $this->t('update File Yml'),
              'url' => Url::fromRoute('block_template_inline.manager', array('updateFile' => $result))
          );
          $operations['remove'] = array(
              'title' => $this->t('Remove'),
              'url' => Url::fromRoute('block_template_inline.manager', array('delete' => $result))
          );
          $operations['removeFile'] = array(
              'title' => $this->t('Remove Yml'),
              'url' => Url::fromRoute('block_template_inline.manager', array('deleteFile' => $result))
          );

          $output[] = [
              'id' => $key+1 ,
              'config_name' =>  $result,
              'operation' => array('data' => array('#type' => 'operations', '#links' => $operations)),
          ];

      }

      $form['table'] = array(
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $output,
          '#empty' => $this->t('No variables found')
      );

    return $form ;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
