<?php

namespace Drupal\block_template_inline\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Edit config variable form.
 */
class ConfigTemplateCreate extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'config_template_create_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        $block_type_lists = \Drupal::service('entity.manager')->getStorage('block_content_type')->loadMultiple();
        $block_type = [];
        foreach ($block_type_lists as $key => $type){
            $block_type[$key] = $type->label();
        }
        $form['type'] = [
            '#type' => 'select',
            '#title' => $this->t('Block Type'),
            '#options' => $block_type,
            '#required' => TRUE
        ];
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
        ];
        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to Template list'),
            '#url' => $this->buildCancelLinkUrl(),
        );

        return $form;
    }

    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */
    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;
        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('block_template_inline.manager');
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        if ($values['type']) {
            $config_name_init = $values['type'];
            $names = $this->configFactory()->listAll("template.");
            $config_name = "template.".trim($config_name_init) ;
            if(in_array($config_name,$names)){
                $this->messenger()->addError($this->t('Template name '.$config_name.' exist already '));
            } else {
                $this->configFactory()->getEditable($config_name)
                    ->set('content','')
                    ->set('type',$config_name_init)
                    ->save();
              $this->messenger()->addMessage($this->t('Template created was successfully'));
            }
        }
    }

}
