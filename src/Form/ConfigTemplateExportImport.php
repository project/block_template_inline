<?php

namespace Drupal\block_template_inline\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
/**
 * Edit config variable form.
 */
class ConfigTemplateExportImport extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'config_template_export_import_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        $module_list = \Drupal::service('extension.list.module')->getList();
        $options =[];
        $config_name = "block_template_inline.settings" ;
        $config = \Drupal::config($config_name) ;
        $output =  $config->get('location');
        foreach ($module_list as $key_mod => $mod){
            if ($mod->origin !='core' && strpos($mod->subpath, 'modules/contrib') === false && $mod->status == 1) {
              $options[$key_mod] = $mod->getName() ;
            }
        }
        $form['location'] = [
            '#type' => 'select',
            '#title' => $this->t('Module location'),
            '#options' => $options,
            '#description' => 'Make sure to have permission to read the Module config ( PATH_MODULE/config/install )',
            '#required' => TRUE,
            '#default_value' => $output
        ];

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Export All'),
        ];
        $form['actions']['import'] = [
            '#type' => 'submit',
            '#value' => $this->t('Import All'),
            '#submit' => ['::submitImport'],
        ];

        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to list'),
            '#url' => $this->buildCancelLinkUrl(),
        );

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function submitImport(array &$form, FormStateInterface $form_state)
    {

            $values = $form_state->getValues();
            if ($values['location']) {
                $config_name = "block_template_inline.settings" ;
                $location =  $values['location'];
                $this->configFactory()->getEditable($config_name)
                    ->set('location',$location)
                    ->save();
                if (\Drupal::moduleHandler()->moduleExists($location)) {
                    $module_handler = \Drupal::service('module_handler');
                    $path_module =  $module_handler->getModule($location)->getPath();
                    $fileSystem = \Drupal::service('file_system');
                    if (!is_dir($path_module . '/config/install')) {
                        $this->messenger()->addError('Config  directory : ' . $path_module . '/config/install   not exist' );
//                        if ($fileSystem->mkdir($path_module . '/config/install', 0777, TRUE) === FALSE) {
//                            $this->messenger()->addError('Failed to create directory ' . $path_module . '/config/install');
//                        }else{
//                            \Drupal::service('config.installer')->installDefaultConfig('module',    $location);
//                            $this->messenger()->addMessage($this->t('Config importation was successfully'));
//                        }
                    }else{
                        \Drupal::service('config.installer')->installDefaultConfig('module',    $location);
                        $this->messenger()->addMessage($this->t('Config importation ' . $path_module . '/config/install was successfully'));
                    }

                }else{
                    $this->messenger()->addError('Failed to Import Templates');
                }

            }
    }

    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */
    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;
        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('block_template_inline.manager');
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        if ($values['location']) {
            $config_name = "block_template_inline.settings" ;
            $location =  $values['location'];
            $this->configFactory()->getEditable($config_name)
                ->set('location',$location)
                ->save();
            if (\Drupal::moduleHandler()->moduleExists($location)) {
                $module_handler = \Drupal::service('module_handler');
                $path_module =  $module_handler->getModule($location)->getPath();
                $fileSystem = \Drupal::service('file_system');
                $path_module = DRUPAL_ROOT."/".$path_module."/config/install";
                if (is_dir($path_module . '/config/install')) {
                    $results = $this->configFactory()->listAll("template.");
                    foreach ($results as $key => $result) {
                        $config = \Drupal::config($result) ;
                        $data = $config->getOriginal();
                        try {
                            $output = Yaml::encode($data);
                            $file = $path_module.'/'.$result.'.yml' ;
                            file_put_contents($file, $output);
                        }
                        catch (InvalidDataTypeException $e) {
                            $this->messenger()->addError($this->t('Invalid data detected for @name : %error', array('@name' => $config_name, '%error' => $e->getMessage())));
                            return;
                        }


                    }
                }else{
                        if ($fileSystem->mkdir($path_module . '/config/install', 0777, TRUE) === FALSE) {
                            $this->messenger()->addError('Failed to create directory ' . $path_module . '/config/install');
                        }else{
                            $results = $this->configFactory()->listAll("template.");
                            foreach ($results as $key => $result) {
                                $config = \Drupal::config($result) ;
                                $data = $config->getOriginal();
                                try {
                                    $output = Yaml::encode($data);
                                    $file = $path_module.'/'.$result.'.yml' ;
                                    file_put_contents($file, $output);
                                }
                                catch (InvalidDataTypeException $e) {
                                    $this->messenger()->addError($this->t('Invalid data detected for @name : %error', array('@name' => $result, '%error' => $e->getMessage())));
                                    return;
                                }


                            }
                            $this->messenger()->addMessage($this->t('Config importation was successfully'));
                        }
                }
            }

        }
    }

}
